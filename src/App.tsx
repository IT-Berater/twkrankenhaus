//
// Thomas Wenzlaff
//

// import Daten
import Data from "./krhs.json";
import "./App.css";
import { useState } from "react";
// Grüner Hacken im Kreis
import CheckCircleIcon from "@patternfly/react-icons/dist/esm/icons/check-circle-icon";
// Rotes Kreuz im Kreis
import TimesCircleIcon from "@patternfly/react-icons/dist/js/icons/times-circle-icon";
// Copy Funktionen
import { ClipboardCopy } from "@patternfly/react-core";
import {
  Card,
  CardTitle,
  CardBody,
  CardFooter,
  Gallery,
  DescriptionList,
  DescriptionListGroup,
  DescriptionListTerm,
  DescriptionListDescription,
  Divider,
  Flex,
  FlexItem,
} from "@patternfly/react-core";

function isKooperativ(wert: boolean): JSX.Element {
  if (wert) {
    return <CheckCircleIcon color="var(--pf-global--success-color--100)" />;
  } else {
    return <TimesCircleIcon color="var(--pf-global--danger-color--100)" />;
  }
}

export default function App(): JSX.Element {
  const [query, setQuery] = useState("");
  return (
    <>
      <div className="sucheleiste">
        <p>Krankenhaus Suche</p>
        <input
          placeholder="Suchbegriff eingeben"
          onChange={(event) => setQuery(event.target.value)}
        />
      </div>
      <Gallery>
        {Data.filter((post) => {
          if (query === "") {
            return post;
          } else if (
            post.krhsName.toLowerCase().includes(query.toLowerCase()) ||
            post.krhsSuchbegriffe.toLowerCase().includes(query.toLowerCase())
          ) {
            return post;
          }
          return false;
        }).map((post, index) => (
          <div key={index}>
            <Card>
              <CardTitle>
                {post.krhsId}.{" "}
                {
                  <ClipboardCopy
                    hoverTip="Kopieren"
                    clickTip="kopiert"
                    variant="inline-compact"
                  >
                    {post.krhsName}
                  </ClipboardCopy>
                }
              </CardTitle>
              <CardBody>
                <DescriptionList>
                  <DescriptionListGroup>
                    <DescriptionListTerm>
                      <Flex flexWrap={{ default: "nowrap" }}>
                        <FlexItem>
                          {isKooperativ(post.krhsKooperativ)}{" "}
                          {post.krhsFachgebiete}
                        </FlexItem>
                      </Flex>
                    </DescriptionListTerm>
                  </DescriptionListGroup>
                  <Divider />
                  <DescriptionListGroup>
                    <DescriptionListTerm>{post.krhsStr}</DescriptionListTerm>
                    <DescriptionListTerm>
                      {post.krhsPlz} {post.krhsOrt}
                    </DescriptionListTerm>
                    <DescriptionListDescription>
                      <a href={post.krhsUrl}>Link ...</a>
                    </DescriptionListDescription>
                  </DescriptionListGroup>
                </DescriptionList>
              </CardBody>
              <Divider />
              <CardFooter>{post.krhsSuchbegriffe}</CardFooter>
            </Card>
          </div>
        ))}
      </Gallery>
    </>
  );
}
